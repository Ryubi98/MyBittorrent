#include "file.h"

int get_file_size(char *file)
{
    struct stat s = { 0 };
    if (stat(file, &s) == 0)
        return s.st_size;
    return 0;
}

int file_exists(char *file)
{
    struct stat s = { 0 };

    if (stat(file, &s) == 0)
        return 1;
    return 0;
}