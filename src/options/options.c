#include "options.h"
#include "file.h"
#include "torrent.h"

static struct options *init_options(void)
{
    struct options *options = calloc(NB_OPTIONS, sizeof(struct options));
    if (options == NULL)
    {
        fprintf(stderr, "Cannot do memory allocation\n");
        exit(1);
    }

    static char *short_opt[] =
    {
        "-p",
        "-m",
        "-c",
        "-d",
        "-v"
    };
    static char *long_opt[] =
    {
        "--pretty-print-torrent-file",
        "--mktorrent",
        "--check-integrity",
        "--dump-peers",
        "--verbose"
    };

    for (int i = 0; i < NB_OPTIONS; i++)
    {
        options[i].type = i;
        options[i].short_name = short_opt[i];
        options[i].long_name = long_opt[i];
    }

    return options;
}

static int check_short(struct options *options, char *option)
{
    for (int i = 0; i < NB_OPTIONS; i++)
    {
        if (strcmp(options[i].short_name, option) == 0)
        {
            if (options[i].active)
                return 1;
            if (i < COMBINED_OPTIONS)
            {
                for (int j = 0; j < COMBINED_OPTIONS; j++)
                {
                    if (options[j].active)
                        return 1;
                }
            }
            options[i].active = 1;
            return 0;
        }
    }

    return 1;
}

static int check_long(struct options *options, char *option)
{
    for (int i = 0; i < NB_OPTIONS; i++)
    {
        if (strcmp(options[i].long_name, option) == 0)
        {
            if (options[i].active)
                return 1;
            if (i < COMBINED_OPTIONS)
            {
                for (int j = 0; j < COMBINED_OPTIONS; j++)
                {
                    if (options[j].active)
                        return 1;
                }
            }
            options[i].active = 1;
            return 0;
        }
    }

    return 1;
}

static int check_options(struct options *options, char *option)
{
    if (option[1] == '-')
        return check_long(options, option);
    else
        return check_short(options, option);
}

int check_arguments(int argc, char *argv[], struct options **options)
{
    *options = init_options();

    for (int i = 1; i < argc; i++)
    {
        if (argv[i][0] == '-')
        {
            if (check_options(*options, argv[i]))
            {
                fprintf(stderr, "Invalid option\n");
                exit(1);
            }
        }
        else
            return i;
    }

    free(*options);
    char *bin = "my-bittorrent";
    fprintf(stderr, "%s: Usage: ./%s [options] [files]\n", bin, bin);
    exit(1);
}

static int dump_peers(void)
{
    return 0;
}

int execute_options(char *argv[], struct options *options, int index_file)
{
    size_t size = get_file_size(argv[index_file]);

    if (options[PRINT].active)
        return pretty_print(argv, size, index_file);

    int res = 0;
    if (options[DUMP].active)
        res = res || dump_peers();
    if (options[MKTORRENT].active)
        res = res || mktorrent_opt(argv[index_file], &size);
    if (options[CHECK].active)
        res = res || check_integrity(argv, index_file);
    //contact tracker, peers ect

    return res;
}
