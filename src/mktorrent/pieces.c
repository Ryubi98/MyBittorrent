#include "torrent.h"
#include <openssl/sha.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

char *mid_pieces(char *path, size_t *size)
{
    size_t sz = 262144;
    char *res = calloc(20 * (*size / 262144 + 1) + 1, sizeof(char));
    int fd = open(path, O_RDONLY);
    for (size_t i = 0; i < *size; i += 262144)
    {
        if (*size - i < 262144)
            sz = *size - i;
        unsigned char blu[21];
        void *data = mmap(NULL, sz, PROT_READ, MAP_PRIVATE,
            fd, i);
        SHA1(data, sz, blu);
        blu[20] = '\0';
        void *tmp = blu;
        char *tmp2 = tmp;
        strncpy(res + i, tmp2, 20);
    }
    close(fd);
    return res;
}
