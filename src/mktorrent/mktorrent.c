#include "torrent.h"
#include <openssl/sha.h>

static struct be_string *alloc_bestr(char *content)
{
    struct be_string *new = malloc(sizeof(struct be_string));
    if (!new)
        return NULL;

    new->length = strlen((const char *) content);
    new->content = calloc(strlen(content) + 1, sizeof(char));
    strcpy(new->content, content);
    return new;
}

static struct be_dict *alloc_bedict_str(char *name, char *content)
{
    struct be_dict *new_dct = calloc(1, sizeof(struct be_dict));
    if (!new_dct)
        return NULL;

    new_dct->key = alloc_bestr(name);
    new_dct->val = be_alloc(BE_STR);
    new_dct->val->element.str = alloc_bestr(content);
    return new_dct;
}

static struct be_dict *alloc_bedict_int(char *name, long long int dgt)
{
    struct be_dict *new_dct = calloc(3, sizeof(struct be_dict));
    if (!new_dct)
        return NULL;

    new_dct->key = alloc_bestr(name);
    new_dct->val = be_alloc(BE_INT);
    new_dct->val->element.num = dgt;
    return new_dct;
}

static struct be_dict **alloc_dictinfo(char *name, char *path, size_t *size)
{
    struct be_dict **info_dict = calloc(5, sizeof(struct be_dict *));
    if (!info_dict)
        return NULL;

    info_dict[0] = alloc_bedict_int("length", 11);
    info_dict[1] = alloc_bedict_str("name", name);
    info_dict[2] = alloc_bedict_int("piece length", 262144);
    char *res = mid_pieces(path, size);
    info_dict[3] = alloc_bedict_str("pieces", res);
    free(res);
    return info_dict;
}

static struct be_dict *alloc_bedict_dict(char *name, char *path, size_t *size)
{
    struct be_dict *new_dct = calloc(3, sizeof(struct be_dict));
    if (!new_dct)
        return NULL;

    new_dct->key = alloc_bestr(name);
    new_dct->val = be_alloc(BE_DICT);
    new_dct->val->element.dict = alloc_dictinfo(name, path, size);
    return new_dct;
}

static struct be_dict **alloc_dictlist(char *path, size_t *size)
{
    struct be_dict **l_dict = calloc(6, sizeof(struct be_dict *));
    if (!l_dict)
        return NULL;

    l_dict[0] = alloc_bedict_str("announce", "http://localhost:6969/announce");
    l_dict[1] = alloc_bedict_str("comment", "<whatever>");
    l_dict[2] = alloc_bedict_str("created by", "<login_x>");
    l_dict[3] = alloc_bedict_int("creation date", 19122018);
    l_dict[4] = alloc_bedict_dict("info", path, size);
    return l_dict;
}

int mktorrent_opt(char *name, size_t *fsize)
{
    struct be_node *new = be_alloc(BE_DICT);
    if (!new)
        return 1;

    char *new_name = calloc(strlen(name) + 9, sizeof(char));
    for (size_t i = 0; i < strlen(name); i++)
        new_name[i] = name[i];
    sprintf(new_name + strlen(name), ".torrent");
    FILE *tfile = fopen(new_name, "w");
    new->element.dict = alloc_dictlist(name, fsize);
    char *data = be_encode(new, fsize);
    fprintf(tfile, data);
    free(data);
    free(new_name);
    be_free(new);
    fclose(tfile);
    return 0;
}
