#include "torrent.h"

static int max_elm(struct be_node *code)
{
    int max = 0;
    for (size_t i = 0; code->element.dict[i] != NULL; i++)
        max++;
    return max;
}

static void print_space(int dic)
{
    for (int k = 0; k < dic; k++)
        printf("    ");
}

static void print_str(char *str, int *cpt, int i, int max, int dic, int len)
{
    printf("\"");
    while (*cpt < len)
    {
        if (str[*cpt] < 32 || str[*cpt] > 126)
        {
            unsigned char a = str[*cpt];
            printf("%s", "U+00");
            printf("%02X", a);
        }
        else
        {
            if (str[*cpt] == '\\' || str[*cpt] == '\"')
                printf("\\%c", str[*cpt]);
            else
                printf("%c", str[*cpt]);
        }
        *cpt += 1;
    }
    if (i + 1 < max)
    {
        printf("\",\n");
        print_space(dic);
    }
    else
    {
        printf("\"\n");
        print_space(dic - 1);
    }
}


void print_bencode2(struct be_node *code, char *name, int i, int max, int dic)
{
    if (code->type == BE_DICT)
    {
        max = max_elm(code);
        if (i == 0)
        {
            printf("{\n");
            print_space(dic + 1);
        }
        else
        {
            printf("\"%s\": {\n", name);
            print_space(dic + 1);
        }
        for (size_t i = 0; code->element.dict[i] != NULL; i++)
        {
            print_bencode2(code->element.dict[i]->val,
                code->element.dict[i]->key->content, i, max, dic + 1);
        }
        printf("}\n");
    }
    else if (code->type == BE_STR)
    {
        int cpt = 0;
        char *str = code->element.str->content;
        printf("\"%s\": ", name);
        print_str(str, &cpt, i, max, dic, code->element.str->length);
    }
    else if (code->type == BE_INT)
    {
        printf("\"%s\": \"%lld\",\n", name, code->element.num);
        if (i + 1 < max)
            print_space(dic);
        else
            print_space(dic - 1);
    }
    else
        return;
}

int pretty_print(char **argv, size_t fsize, int index_file)
{
    FILE *file = fopen(argv[index_file], "r");
    char *str = malloc(sizeof(char) * (fsize + 1));
    if (str == NULL)
    {
        fclose(file);
        return 1;
    }
    data_torrent(str, file);
    struct be_node *code = be_decode(str, fsize);
    if (code == NULL)
    {
        free(str);
        fprintf(stderr, "Error, this file is not a torrent file\n");
        return 1;

    }
    print_bencode2(code, NULL, 0, 0, 0);
    free(str);
    be_free(code);
    return 0;
}
