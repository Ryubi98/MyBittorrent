#include "torrent.h"

void data_torrent(char *dest, FILE *file)
{
    if (!file || !dest)
        return;

    int ind = 0;
    int ltr = fgetc(file);
    while (ltr != EOF)
    {
        dest[ind] = ltr;
        ltr = fgetc(file);
        ind++;
    }
    dest[ind] = '\0';
    fclose(file);
}
