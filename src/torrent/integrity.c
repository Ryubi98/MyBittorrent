#include "torrent.h"
#include "file.h"

static struct be_node *check_key(struct be_node *node, char *key)
{
    if (node->type != BE_DICT)
        return NULL;
    for (int i = 0; node->element.dict[i]; i++)
    {
        if (strcmp(node->element.dict[i]->key->content, key) == 0)
            return node->element.dict[i]->val;
    }
    return NULL;
}

static int check_file(struct be_node *code)
{
    struct be_node *info = check_key(code, "info");
    if (info == NULL)
        return 1;
    struct be_node *name = check_key(info, "name");
    if (name == NULL)
        return 1;
    return !file_exists(name->element.str->content);
}

int check_integrity(char *argv[], int index_file)
{
    FILE *file = fopen(argv[index_file], "r");
    if (file == NULL)
        return 1;

    size_t fsize = get_file_size(argv[index_file]);
    char *str = malloc(sizeof(char) * (fsize + 1));
    if (str == NULL)
    {
        fclose(file);
        fprintf(stderr, "Cannot do memory allocation\n");
        return 1;
    }
    data_torrent(str, file);
    struct be_node *code = be_decode(str, fsize);
    if (code == NULL)
    {
        free(str);
        fprintf(stderr, "Error this file is not a torrent file\n");
        return 1;
    }
    int res = check_file(code);
    free(str);
    be_free(code);
    return res;
}
