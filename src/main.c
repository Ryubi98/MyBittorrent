#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "options.h"
#include "file.h"
#include "bencode.h"
#include "torrent.h"

int main(int argc, char *argv[])
{
    struct options *options = NULL;
    int index_file = check_arguments(argc, argv, &options);

    if (!file_exists(argv[index_file]))
    {
        free(options);
        fprintf(stderr, "The file %s does not exist\n", argv[index_file]);
        exit(1);
    }

    int res = execute_options(argv, options, index_file);
    free(options);
    return res;
}
