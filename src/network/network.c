#include "network.h"

static size_t write_callback(char *ptr, size_t size, size_t nmemb, void *data)
{
    printf("%s\n", ptr);
    printf("%ld - %ld\n", size, nmemb);
    printf("%s\n", (char *)data);
    return nmemb;
}

int contact_tracker(char *url)
{
    CURL *handle = curl_easy_init();

    char *buf = NULL;
    char errbuff[CURL_ERROR_SIZE];

    curl_easy_setopt(handle, CURLOPT_URL, url);
    curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, write_callback);
    curl_easy_setopt(handle, CURLOPT_WRITEDATA, &buf);
    curl_easy_setopt(handle, CURLOPT_ERRORBUFFER, &errbuff);

    int res = curl_easy_perform(handle);

    curl_easy_cleanup(handle);
    curl_global_cleanup();

    return res;
}

int contact_peer(char *ip)
{
    int fd_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (fd_socket < 0)
    {
        fprintf(stderr, "Cannot open socket\n");
        exit(1);
    }
    (void)ip;
    return 0;
    //todo
}
