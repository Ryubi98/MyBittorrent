#!/bin/sh

cd ./tests/command
for file in *; do
    echo "====================="
    echo ""
    cat $file
    ./$file &> output.txt
    cmp output.txt ../outputs/$file &> trash.txt
    res=$?
    if [ $res -eq 0 ]; then
        echo -e "\033[32mOK\033[0m"
    else
        echo -e "\033[31mFAILED\033[0m"
        echo -n "Excepted: "
        cat ../outputs/$file
    fi
    echo -n "Output: "
    cat output.txt
    echo ""
    echo "====================="
    echo ""
    rm -f *.txt *.torrent
done
rm ../my-bittorrent
