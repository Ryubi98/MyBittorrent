#ifndef OPTIONS_H
#define OPTIONS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NB_OPTIONS 5
#define COMBINED_OPTIONS 3

enum OPTION
{
    PRINT,
    MKTORRENT,
    CHECK,
    DUMP,
    VERBOSE
};

struct options
{
    enum OPTION type;
    char *short_name;
    char *long_name;
    int active;
};

int check_arguments(int argc, char *argv[], struct options **options);

int execute_options(char *argv[], struct options *options, int index_file);

#endif /* ! OPTIONS_H */
