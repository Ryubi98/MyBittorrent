#ifndef MAIN_H
#define MAIN_H

#include "bencode.h"
#include "options.h"

struct opt
{
    int opt_print;
    int opt_c;
    int opt_mk;
    int opt_dump;
    int opt_verbose;
};

void data_torrent(char *dest, FILE *file);
void print_bencode2(struct be_node *code, char *name, int i, int max, int dic);
int pretty_print(char **argv, size_t fsize, int index_file);
int mktorrent_opt(char *name, size_t *fsize);
char *mid_pieces(char *path, size_t *size);
void free_node(struct be_node *node);
int check_integrity(char *argv[], int index_file);

#endif /* ! MAIN_H */
