#ifndef FILES_H
#define FILES_H

#include <sys/stat.h>

int get_file_size(char *file);

int file_exists(char *file);

#endif /* ! FILES_H */